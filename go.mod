module tendermint-signer

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gogo/protobuf v1.3.2
	github.com/stretchr/testify v1.7.0
	github.com/tendermint/go-amino v0.16.0
	github.com/tendermint/tendermint v0.34.3
	gitlab.com/unit410/edwards25519 v0.0.0-20220725154547-61980033348e
	gitlab.com/unit410/threshold-ed25519 v0.0.0-20220725172740-6ee731f539ac
)
